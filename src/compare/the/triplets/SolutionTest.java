package compare.the.triplets;

import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class SolutionTest {

    @Test
    public void bothShouldHaveSameResult() {
        //given
        List<Integer> aliceInput = Lists.newArrayList(5, 6, 7);
        List<Integer> bobInput = Lists.newArrayList(3,6,10);

        //when
        List<Integer> points = Solution.compareTriplets(aliceInput, bobInput);

        //then
        assertEquals(Lists.newArrayList(1, 1), points);
    }
}
